#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <string>
#include <iostream>

#include <stdio.h>

cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);

int main(int argc, char* argv[])
{
	int deviceCount;
	cudaDeviceProp devProp;

	cudaGetDeviceCount(&deviceCount);
	//printf("Found %d devices\n", deviceCount);

	/*int version;
	cudaRuntimeGetVersion(&version);
	printf("Version CUDA : %d\n", version);*/

	for (int device = 0; device < deviceCount; device++)
	{
		cudaGetDeviceProperties(&devProp, device);
		
		//printf("Prop of device :\n");
		printf("Name : %s\n", devProp.name);
		//printf("Total global memory : %u\n", devProp.totalGlobalMem);
		std::cout << "Total global memory : " << devProp.totalGlobalMem << '\n';
		printf("Shared memory per block : %d\n", devProp.sharedMemPerBlock);
		printf("Registers per block : %d\n", devProp.regsPerBlock);
		printf("Warp size : %d\n", devProp.warpSize);
		printf("Memory pitch : %d\n", devProp.memPitch);
		printf("Max threads per block : %u\n", devProp.maxThreadsPerBlock);
		//printf("Max threads dim : %u\n", devProp.maxThreadsDim);
		std::cout << "Max threads dim : " << devProp.maxThreadsDim << '\n';
		//printf("Max grid size : %u\n", devProp.maxGridSize);
		std::cout << "Max grid size : " << devProp.maxGridSize << '\n';
		printf("Total conts memory : %d\n", devProp.totalConstMem);
		printf("Compute capability : %d.%d\n", devProp.major, devProp.minor);
		printf("Clock rate : %d\n", devProp.clockRate);
		printf("Texture alignment : %d\n", devProp.textureAlignment);
		printf("Device overlap : %d\n", devProp.deviceOverlap);
		printf("Multi processor count (SM) : %d\n", devProp.multiProcessorCount);
	}

	return 0;
}